<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
            ['name' => 'Product A', 'quantity' => 1,'unit_price'=>10,'created_at'=>now(),'updated_at'=>now()],
            ['name' => 'Product B', 'quantity' => 2,'unit_price'=>20,'created_at'=>now(),'updated_at'=>now()],
            ['name' => 'Product C', 'quantity' => 3,'unit_price'=>30,'created_at'=>now(),'updated_at'=>now()],
        ]);
    }
}
