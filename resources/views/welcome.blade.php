<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{ mix('/css/app.css')}}">


    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="https://www.visualcv.com/fernando-castillo-english/pdf/" target="_blank">Fernando Castillo</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="https://www.visualcv.com/fernando-castillo-english/pdf/" target="_blank">Resume</a>
                    <a class="nav-item nav-link" href="https://www.linkedin.com/in/jose-fernando-castillo-rosas-a72542117/" target="_blank">Linkedin</a>
                </div>
            </div>
        </nav>

        <div id="app" class="mt-5">
            <app-component></app-component>
        </div>
        <script src="{{mix('/js/app.js')}}"></script>
    </body>
</html>
