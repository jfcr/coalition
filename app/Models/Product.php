<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $guarded = ['created_at','updated_at'];
    protected $appends = ['total'];

    public function getTotalAttribute(){
        return $this->quantity * $this->unit_price;
    }

}
