<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response(Product::all()->jsonSerialize(),Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        //
        $product = Product::create([
            'name'          =>  $request->name,
            'quantity'      =>  $request->quantity,
            'unit_price'    =>  $request->unit_price,
        ]);

        return response($product->jsonSerialize(), Response::HTTP_CREATED);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        //
        $product = Product::findOrFail($id);
        $product->name          = $request->name;
        $product->quantity      = $request->quantity;
        $product->unit_price    = $request->unit_price;
        $product->save();

        return response($product->jsonSerialize(), Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Product::destroy($id);
        return response(null, Response::HTTP_OK);
    }
}
