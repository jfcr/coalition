# Coalition Laravel 5.8 Code Challenge

Clone this repository and install

```sh
$ cd coalition
$ composer install
```

Copy .env.example to .env to setup your initial DB.

Run migrations and seeders

```sh
$ php artisan migrate --seeder
```

Run server

```sh
$ php artisan serve
```

Check the product crud.
![Screenshot](screenshot.png)